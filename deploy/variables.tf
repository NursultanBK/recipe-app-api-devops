variable "prefix" {
  default     = "raad"
  description = "recipe app API DevOps"
}

variable "project" {
  default = "recipe-app-api-devops"
}

variable "contact" {
  default = "yurtahconstruction@gmail.com"
}
