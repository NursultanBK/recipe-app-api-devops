terraform {
  required_version = ">= 0.12"
}

terraform {
  backend "s3" {
    bucket         = "yurtahconstruction"
    key            = "recipe-app.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "recipe-app-api-devops-tf-state-lock"
  }
}

provider "aws" {
  region = "us-east-1"
}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common_tags = {
    Environment = terraform.workspace
    Project     = var.project
    Ownew       = var.contact
    ManagedBy   = "Terraform"
  }
}

 